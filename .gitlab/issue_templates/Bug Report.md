<!--
  Before opening an issue, check that it hasn't already been reported:
    ↪ https://gitlab.com/BVollmerhaus/blurwal/issues?label_name[]=Bug
-->

## Summary
<!-- Concisely summarize the issue you encountered. -->


## Environment
<!--
  What environment did you encounter the issue in?

  Note that only the latest release and development version are supported.
  If you are using an older version, please update to the current release
  first to check if your issue is still reproducible there.
-->

* BlurWal version: `1.0.2`
* ImageMagick version: ``
* feh version: ``
* Window Manager: 


## Current Behavior
<!-- Describe the current behavior. -->


## Expected Behavior
<!-- Describe the behavior you expect. -->


## Reproduction Steps
<!-- Summarize the steps for reproducing the issue. -->

1. ...
1. ...


## Attachments
<!--
  If available: Additional information that may be helpful in fixing
  the issue, such as log files, config files, screenshots, or videos

  Please use code blocks (```) to format logs, terminal output, etc.
-->

–


/label ~Bug
