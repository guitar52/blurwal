"""
Test cases for the path constants module.

Author: Benedikt Vollmerhaus
License: MIT
"""

import pathlib

from blurwal import paths


def test_get_original(mocker, datadir):
    mocker.patch('blurwal.paths.ORIGINAL_PATH', datadir / 'original-path')
    assert (paths.get_original() ==
            pathlib.Path('/home/user/images/original.jpg'))


def test_set_original(mocker, datadir):
    mocker.patch('blurwal.paths.ORIGINAL_PATH', datadir / 'original-path')
    paths.set_original(pathlib.Path('/home/user/images/wallpaper.jpg'))
    assert (paths.get_original() ==
            pathlib.Path('/home/user/images/wallpaper.jpg'))
