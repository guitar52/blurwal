"""
Test cases for the windows module.

Author: Benedikt Vollmerhaus
License: MIT
"""

from unittest.mock import create_autospec

import pytest
import Xlib

from blurwal import windows, x11


def test_count_open(x_connection_mock):
    assert windows.count_open([], x_connection_mock) == 3


def test_count_open_does_not_count_ignored(x_connection_mock):
    assert windows.count_open(['URxvt'], x_connection_mock) == 2


@pytest.fixture(scope='module')
def x_connection_mock():
    window_mocks = []
    for window_class in ('Firefox', 'Riot', 'URxvt'):
        window = create_autospec(Xlib.display.drawable.Window)
        window.get_wm_class.return_value = ['', window_class]
        window_mocks.append(window)

    mock = create_autospec(x11.XConnection)
    mock.get_windows_on_current_workspace.return_value = window_mocks
    return mock
