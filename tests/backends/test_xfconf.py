"""
Test cases for the xfconf backend.

Author: Benedikt Vollmerhaus
License: MIT
"""

import pathlib
from subprocess import CalledProcessError
from unittest.mock import call

import pytest

from blurwal.backends import xfconf


def test_find_wallpaper_properties(mocker):
    mocker.patch('subprocess.check_output', return_value=(
        b'/backdrop/screen0/monitorLVDS1/workspace0/color-style\n'
        b'/backdrop/screen0/monitorLVDS1/workspace0/last-image\n'
        b'/backdrop/screen0/monitorLVDS1/workspace1/image-style\n'
        b'/backdrop/screen0/monitorLVDS1/workspace1/last-image'
    ))

    assert xfconf.find_wallpaper_properties() == [
        '/backdrop/screen0/monitorLVDS1/workspace0/last-image',
        '/backdrop/screen0/monitorLVDS1/workspace1/last-image'
    ]


def test_change_to_runs_xfconf_query(mocker, xfconf_backend):
    wallpaper_path = pathlib.Path('/home/user/images/wallpaper.jpg')
    run_mock = mocker.patch('subprocess.run')
    xfconf_backend.change_to(wallpaper_path)

    run_mock.assert_has_calls([
        call(['xfconf-query',
              '-c', 'xfce4-desktop',
              '-p', '/backdrop/screen0/monitorLVDS1/workspace0/last-image',
              '-s', str(wallpaper_path)]),
        call(['xfconf-query',
              '-c', 'xfce4-desktop',
              '-p', '/backdrop/screen0/monitorLVDS1/workspace1/last-image',
              '-s', str(wallpaper_path)])
    ])
    assert run_mock.call_count == 2


def test_get_current(mocker, xfconf_backend):
    mocker.patch('subprocess.check_output',
                 return_value=b'/home/user/images/wallpaper.jpg')
    assert (xfconf_backend.get_current() ==
            pathlib.Path('/home/user/images/wallpaper.jpg'))


def test_get_current_exits_when_xfconf_non_zero(mocker, xfconf_backend):
    mocker.patch('subprocess.check_output',
                 side_effect=CalledProcessError(
                     returncode=1, cmd='xfconf-query'))
    with pytest.raises(SystemExit):
        xfconf_backend.get_current()


def test_restore_original(mocker, datadir, xfconf_backend):
    mocker.patch('blurwal.paths.ORIGINAL_PATH', datadir / 'original-path')
    expected_original_path = pathlib.Path('/home/user/images/original.jpg')

    run_mock = mocker.patch('subprocess.run')
    xfconf_backend.restore_original()

    run_mock.assert_has_calls([
        call(['xfconf-query',
              '-c', 'xfce4-desktop',
              '-p', '/backdrop/screen0/monitorLVDS1/workspace0/last-image',
              '-s', str(expected_original_path)]),
        call(['xfconf-query',
              '-c', 'xfce4-desktop',
              '-p', '/backdrop/screen0/monitorLVDS1/workspace1/last-image',
              '-s', str(expected_original_path)])
    ])
    assert run_mock.call_count == 2


def test_restore_original_exits_when_missing(mocker, datadir, xfconf_backend):
    mocker.patch('blurwal.paths.ORIGINAL_PATH', datadir / 'non-existent')
    with pytest.raises(SystemExit):
        xfconf_backend.restore_original()


@pytest.fixture
def xfconf_backend(mocker):
    mocker.patch('blurwal.backends.xfconf.x11.XConnection')
    mocker.patch('blurwal.backends.xfconf.find_wallpaper_properties',
                 return_value=[
                     '/backdrop/screen0/monitorLVDS1/workspace0/last-image',
                     '/backdrop/screen0/monitorLVDS1/workspace1/last-image'
                 ])

    return xfconf.XfconfBackend()
