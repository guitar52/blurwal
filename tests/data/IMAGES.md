Images used as test data by:

* [Tania Alieksanenko](https://unsplash.com/@broken_statue) (Mushroom under pine needles)
* [Noah Silliman](https://unsplash.com/@noahsilliman) (Aerial of abandoned shack in forest)
