"""
Author: Joshua Robinson
License: MIT
"""

import logging
import pathlib
import subprocess
import sys
from typing import List, Optional

from blurwal.backends import base


class GsettingsBackend(base.Backend):
    """
    Wallpaper operations utilizing gnome/budgie's gsettings backend.
    """

    def get_wallpaper_set_cmds(self, path: pathlib.Path) -> List[List[str]]:
        """
        Return a list of commands to be executed to set the wallpaper.

        :param path: The image to set as the wallpaper
        :return: A list of commands to be executed
        """
        return [['gsettings',
                'set', 'org.gnome.desktop.background',
                 'picture-uri', 'file://' + str(path)]]

    def get_current(self) -> Optional[pathlib.Path]:
        """
        Return the current wallpaper's path from gsettings

        :return: The current wallpaper's path (formatted for gsettings) or
        None if not retrievable
        """
        try:
            process = subprocess.check_output(['gsettings',
                                               'get',
                                               'org.gnome.desktop.background',
                                               'picture-uri'])

            filepath = process.strip().decode().replace("'file://", "", 1)[:-1]

            return pathlib.Path(filepath)
        except subprocess.CalledProcessError:
            logging.exception('Could not retrieve current wallpaper, call to'
                              'gsettings returned a non-zero exit status.')
            sys.exit(1)
