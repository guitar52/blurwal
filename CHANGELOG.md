# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]


## [1.1.1] - 2019-03-30
### Added
* Automated AUR release in CI

### Changed
* Better startup behavior if the current wallpaper is not retrievable
* Improved logging output with colored single-letter level prefixes

### Fixed
* Crash on retrieving the visibility (e.g. minimized) of bad windows


## [1.1.0] - 2019-03-23
### Added
* Support for implementing additional wallpaper setters (backends)
* A backend for the [**Xfce**](https://www.xfce.org/) desktop environment
  (named `xfce` in CLI)

### Fixed
* Crash when the current wallpaper cannot be extracted from `.fehbg`
* The frame renderer's processing pool not being shut down gracefully

### Removed
* Dependency on [pyewmh](https://github.com/parkouss/pyewmh) (previously
  used for retrieving X properties)


## [1.0.3] - 2019-02-22
### Fixed
* Memory leak due to creating an excessive number of X server connections


## [1.0.2] - 2019-02-18
### Fixed
* Wallpaper path retrieval from fully quoted `.fehbg` files

  Standard file structure (`feh 3.1.2`):
  ```sh
  feh --bg-fill '/path/to/wallpaper.png'
  ```

  Presumably older structure that didn't work previously:
  ```sh
  'feh' '--bg-fill' '/path/to/wallpaper.png'
  ```


## [1.0.1] - 2019-01-28
### Fixed
* Crash on retrieving an invalid window's class


## [1.0.0] - 2018-11-28
**Initial Release**


[Unreleased]: /../compare/1.1.1...master
[1.1.1]: /../compare/1.1.0...1.1.1
[1.1.0]: /../compare/1.0.3...1.1.0
[1.0.3]: /../compare/1.0.2...1.0.3
[1.0.2]: /../compare/1.0.1...1.0.2
[1.0.1]: /../compare/1.0.0...1.0.1
[1.0.0]: /../tags/1.0.0
